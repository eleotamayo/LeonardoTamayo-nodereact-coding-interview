import React, { FC, useState, useEffect } from "react";
import { FixedSizeList as List, ListChildComponentProps } from "react-window";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress, Button, TextField } from "@mui/material";

import { BackendClient } from "../clients/backend.client";

const backendClient = new BackendClient();

const ItemRenderer: FC<ListChildComponentProps> = ({ index, data }) => {
  return (
    <UserCard key={index} {...data[index]} />
  );
}

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  //const loading = true;
  const [loading, setLoading] = useState<boolean>(true)
  const [page, setPage] = useState<number>(1)


  useEffect(() => {
    const fetchData = async () => {
      const result = await backendClient.getUsers(page);
      setUsers(result.data);
      setLoading(false)
    };
    fetchData();

  }, [page]);

  const changePage = (nextPage: number) =>
    setPage(nextPage)

  return (
    <div style={{ paddingTop: "30px" }}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        {loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <List
            itemData={users}
            itemCount={users.length}
            itemSize={10}
            height={600}
            width={"100%"}
          >
            {ItemRenderer}
          </List>
        )}
      </div>
      <Button disabled={page === 1} variant="outlined" onClick={() => changePage(page - 1)}>Atras</Button>
      <Button variant="outlined" onClick={() => changePage(page + 1)}>Adelante</Button>
    </div>
  );
};
