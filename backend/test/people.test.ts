
import { strict as assert } from 'assert';
import { PeopleProcessing } from "../src/services/people_processing.service";

const people_processing = new PeopleProcessing();

describe('Services', function () {
  describe('#people', function () {
    it('all people ', function () {

      const allPeople = people_processing.getAll();
      assert.equal(allPeople.length, 1000);

    });

    it('paginated people', function () {
      const paginatedPeople = people_processing.paginate(1, 10);
      assert.equal(paginatedPeople.length, 10)
    })

  });
});