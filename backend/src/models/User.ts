import mongoose from 'mongoose';

const TestSchema = new mongoose.Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    last_name: {
        type: String,
        unique: true,
        required: true
    },
});

export default mongoose.model("Test", TestSchema);