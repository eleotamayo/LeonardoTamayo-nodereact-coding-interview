import {
    JsonController,
    Get,
    HttpCode,
    NotFoundError,
    Param,
    QueryParam,
} from 'routing-controllers';
import { PeopleProcessing } from '../services/people_processing.service';

const peopleProcessing = new PeopleProcessing();

@JsonController('/people', { transformResponse: false })
export default class PeopleController {
    @HttpCode(200)
    @Get('/all')
    getAllPeople() {
        const people = peopleProcessing.getAll();

        if (!people) {
            throw new NotFoundError('No people found');
        }

        return {
            data: people,
        };
    }

    @HttpCode(200)
    @Get('/')
    paginatedPeople(@QueryParam("page") page: number = 1, @QueryParam("limit") limit: number = 10) {
        const paginatedPeople = peopleProcessing.paginate(page, limit)
        return {
            data: paginatedPeople,
            meta: {
                page,
                limit
            }
        }
    }

    @HttpCode(200)
    @Get('/:id')
    getPerson(@Param('id') id: number) {
        const person = peopleProcessing.getById(id);

        if (!person) {
            throw new NotFoundError('No person found');
        }

        return {
            data: person,
        };
    }
}
