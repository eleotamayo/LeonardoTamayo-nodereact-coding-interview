import people_data from '../data/people_data.json';

export class PeopleProcessing {
    getById(id: number) {
        return people_data.find((p) => p.id === id);
    }

    getAll() {
        return people_data;
    }

    paginate(page: number = 1, limit: number = 10) {
        const start = (page - 1) * limit
        const end = (page) * limit
        return people_data.slice(start, end)
    }
}
